import pytest


@pytest.mark.asyncio
async def test_get(hub, ctx):
    url = "https://www.solrac.nl"
    # Test search tls_certificate
    search_ret = await hub.exec.tls.certificate.get(ctx, url=url)
    assert search_ret["result"], search_ret["comment"]
    assert search_ret.get("ret")
    resource = search_ret.get("ret")
    assert url == resource.get("url")
    assert "d89e3bd43d5d909b47a18977aa9d5ce36cee184c" == resource.get(
        "sha1_fingerprint"
    )
    assert f"Comodo CA Limited" in resource.get("issuer").get("O")
    assert "2028-12-31 23:59:59" == resource.get("not_after")
    assert "2019-03-12 00:00:00" == resource.get("not_before")
    assert "2" == resource.get("version")
    assert "sha384WithRSAEncryption" == resource.get("signature_algorithm")
    assert 76359301477803385872276235234032301461 == resource.get("serial_number")
    assert "USERTrust RSA Certification Authority" in resource.get("subject").get("CN")


@pytest.mark.asyncio
async def test_list_(hub, ctx):
    url = "https://www.solrac.nl"
    # Test search tls_certificate
    search_ret = await hub.exec.tls.certificate.list(ctx, url=url)
    assert search_ret["result"], search_ret["comment"]
    assert search_ret.get("ret") and len(search_ret.get("ret")) > 1
    certs = search_ret.get("ret")
    for cert in certs:
        assert url == cert.get("url")
        assert cert.get("serial_number")
        assert cert.get("issuer")
        assert "2" == cert.get("version")
        assert cert.get("subject")
        assert cert.get("signature_algorithm")
